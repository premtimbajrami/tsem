#!/usr/bin/python

import getopt
import json
import os
import sys
from os import listdir
from os.path import isfile, join

import numpy as np  # (pip install numpy)
from PIL import Image  # (pip install Pillow)
from shapely.geometry import MultiPolygon, Polygon  # (pip install Shapely)
from skimage import measure  # (pip install scikit-image)


def create_sub_masks(mask_image):
    width, height = mask_image.size

    # Initialize a dictionary of sub-masks indexed by RGB colors
    sub_masks = {}
    for x in range(width):
        for y in range(height):
            # Get the RGB values of the pixel
            pixel = mask_image.getpixel((x, y))[:3]

            # If the pixel is not black...
            if pixel == (0, 0, 255):
                # Check to see if we've created a sub-mask...
                pixel_str = str(pixel)
                sub_mask = sub_masks.get(pixel_str)
                if sub_mask is None:
                   # Create a sub-mask (one bit per pixel) and add to the dictionary
                    # Note: we add 1 pixel of padding in each direction
                    # because the contours module doesn't handle cases
                    # where pixels bleed to the edge of the image
                    sub_masks[pixel_str] = Image.new('1', (width+2, height+2))

                # Set the pixel value to 1 (default is 0), accounting for padding
                sub_masks[pixel_str].putpixel((x+1, y+1), 1)

    return sub_masks


def create_sub_mask_annotation(sub_mask, image_id, category_id, annotation_id, is_crowd):
    # Find contours (boundary lines) around each sub-mask
    # Note: there could be multiple contours if the object
    # is partially occluded. (E.g. an elephant behind a tree)
    contours = measure.find_contours(sub_mask, 0.5, positive_orientation='low')

    segmentations = []
    polygons = []
    for contour in contours:
        # Flip from (row, col) representation to (x, y)
        # and subtract the padding pixel
        for i in range(len(contour)):
            row, col = contour[i]
            contour[i] = (col - 1, row - 1)

        # Make a polygon and simplify it
        poly = Polygon(contour)
        poly = poly.simplify(1.0, preserve_topology=False)
        if not poly.is_empty:
            polygons.append(poly)
            segmentation = np.array(poly.exterior.coords).ravel().tolist()
            segmentations.append(segmentation)

    # Combine the polygons to calculate the bounding box and area
    multi_poly = MultiPolygon(polygons)
    x, y, max_x, max_y = multi_poly.bounds
    width = max_x - x
    height = max_y - y
    bbox = (x, y, width, height)
    area = multi_poly.area

    annotation = {
        'segmentation': segmentations,
        'iscrowd': is_crowd,
        'image_id': image_id,
        'category_id': category_id,
        'id': annotation_id,
        'bbox': bbox,
        'area': area
    }

    return annotation


def main(argv):
    # Define which colors match which categories in the images
    sky_id, facade_id, vegetation_id = [1, 2, 3]
    category_ids = {
        '(0, 0, 255)': sky_id,
        '(255, 0, 0)': facade_id,
        '(0, 255, 0)': vegetation_id,
    }
    datasetdir = ''
    mask_images = []
    annotations = {'images': [], 'annotations': [], 'categories': []}
    is_crowd = 0
    image_id = 1

    if len(argv) == 0:
        print('mask.py <datasetdir>')
        sys.exit(2)
    datasetdir = argv[0]
        
    for f in listdir(datasetdir):
        if isfile(join(datasetdir, f)):
            img = Image.open(join(datasetdir, f))
            width, height = img.size
            file_name = f.replace('_color_mask', '')
            file_name = file_name.replace('png', 'jpg')
            annotation = {
                'license': 0,
                'file_name': file_name,
                'width': width,
                'height': height,
                'id': image_id
            }
            annotations['images'].append(annotation)
            image_id += 1
            mask_images.append(img)
    
    annotation_id = 1
    image_id = 1

    # Create the annotations
    for mask_image in mask_images:
        sub_masks = create_sub_masks(mask_image)
        for color, sub_mask in sub_masks.items():
            category_id = category_ids[color]
            annotation = create_sub_mask_annotation(
                sub_mask, image_id, category_id, annotation_id, is_crowd)
            annotations['annotations'].append(annotation)
            annotation_id += 1
        image_id += 1

    categories = [
        {
            "supercategory": "sykview",
            "id": 1,
            "name": "sky_view_factor"
        }
    ]

    annotations['categories'] = categories

    with open('coco_annotations.json', 'w') as outfile:
        json.dump(annotations, outfile)


if __name__ == "__main__":
    main(sys.argv[1:])
